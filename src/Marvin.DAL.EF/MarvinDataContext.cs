using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Marvin.DAL.EF
{
    /// <summary>
    /// Base read-write data context at SIIS projects
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class MarvinDataContext<T> : MarvinReadOnlyDataContext<T>, IMarvinDataContext  where T: DbContext
    {
        /// <inheritdoc />
        public MarvinDataContext(DbContextOptions<T> options) : base(options)
        {
        }

        /// <inheritdoc />
        public IMarvinDataContextTransaction BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            var transaction = base.Database.BeginTransaction(isolationLevel);
            
            return new MarvinDbTransaction(transaction);
        }

        /// <inheritdoc />
        public async Task<IMarvinDataContextTransaction> BeginTransactionAsync(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            var transaction = await base.Database.BeginTransactionAsync(isolationLevel);
            
            return new MarvinDbTransaction(transaction);
        }

        /// <inheritdoc />
        public void UseTransaction(DbTransaction transaction)
        {
            Database.UseTransaction(transaction);
        }
    }
}