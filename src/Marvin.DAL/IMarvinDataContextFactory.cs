namespace Marvin.DAL
{
    /// <summary>
    /// Data context factory (for <see cref="IMarvinDataContext"/>) with typed returned value.
    /// </summary>
    public interface IMarvinDataContextFactory<out TReadWriteContext, out TReadOnlyContext> : IMarvinDataContextFactory
        where TReadWriteContext : IMarvinDataContext
        where TReadOnlyContext  : IMarvinReadOnlyDataContext
    {
        /// <summary>
        /// Creates context <see cref="TReadWriteContext"/> for data access.
        /// </summary>
        /// <returns>New instance of data context</returns>
        new TReadWriteContext CreateContext();
        
        /// <summary>
        /// Creates context <see cref="TReadWriteContext"/> for data access.
        /// </summary>
        /// <returns>New instance of data context</returns>
        new TReadOnlyContext CreateReadOnlyContext();
    }

    /// <summary>
    /// Data context factory (for <see cref="IMarvinDataContext"/>).
    /// </summary>
    public interface IMarvinDataContextFactory
    {
        /// <summary>
        /// Creates context <see cref="IMarvinDataContext"/> for data access.
        /// </summary>
        /// <returns>New instance of data context</returns>
        IMarvinDataContext CreateContext();
 
        /// <summary>
        /// Creates context <see cref="IMarvinReadOnlyDataContext"/> for data access.
        /// </summary>
        /// <returns>New instance of data context</returns>
        IMarvinReadOnlyDataContext CreateReadOnlyContext();
    }
}