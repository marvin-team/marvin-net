using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marvin.DAL
{
    /// <summary>
    /// Executor of SQL queries.
    /// </summary>
    public interface ISqlExecutor
    {
        /// <summary>
        /// Executes SQl query.
        /// </summary>
        /// <param name="context">Data context for query execution</param>
        /// <param name="sqlTemplate">SQL query (can contains parameters placeholders)</param>
        /// <param name="parameters">Parameters for query</param>
        /// <param name="ignoreNulls">Null params will be ignored</param>
        void Execute(
            IMarvinDataContext context,
            string sqlTemplate, 
            IDictionary<string, object>? parameters = null,
            bool ignoreNulls = false);

        /// <summary>
        /// Executes SQl query.
        /// </summary>
        /// <param name="context">Data context for query execution</param>
        /// <param name="sqlTemplate">SQL query (can contains parameters placeholders)</param>
        /// <param name="parameters">Parameters for query</param>
        /// <param name="ignoreNulls">Null params will be ignored</param>
        Task ExecuteAsync(
            IMarvinDataContext context,
            string sqlTemplate, 
            IDictionary<string, object>? parameters = null,
            bool ignoreNulls = false);

        /// <summary>
        /// Executes SQl query and returns value.
        /// </summary>
        /// <param name="context">Data context for query execution</param>
        /// <param name="sqlTemplate">SQL query (can contains parameters placeholders)</param>
        /// <param name="parameters">Parameters for query</param>
        /// <param name="ignoreNulls">Null params will be ignored</param>
        T QuerySingleOrDefault<T>(
            IMarvinReadOnlyDataContext context,
            string sqlTemplate, 
            IDictionary<string, object>? parameters = null,
            bool ignoreNulls = false);

        /// <summary>
        /// Executes SQl query and returns value.
        /// </summary>
        /// <param name="context">Data context for query execution</param>
        /// <param name="sqlTemplate">SQL query (can contains parameters placeholders)</param>
        /// <param name="parameters">Parameters for query</param>
        /// <param name="ignoreNulls">Null params will be ignored</param>
        Task<T> QuerySingleOrDefaultAsync<T>(
            IMarvinReadOnlyDataContext context,
            string sqlTemplate, 
            IDictionary<string, object>? parameters = null,
            bool ignoreNulls = false);
        
        /// <summary>
        /// Executes SQl query and returns collection of values.
        /// </summary>
        /// <param name="context">Data context for query execution</param>
        /// <param name="sqlTemplate">SQL query (can contains parameters placeholders)</param>
        /// <param name="parameters">Parameters for query</param>
        /// <param name="ignoreNulls">Null params will be ignored</param>
        IEnumerable<T> QueryMany<T>(
            IMarvinReadOnlyDataContext context,
            string sqlTemplate, 
            IDictionary<string, object>? parameters = null,
            bool ignoreNulls = false);

        /// <summary>
        /// Executes SQl query and returns collection of value.
        /// </summary>
        /// <param name="context">Data context for query execution</param>
        /// <param name="sqlTemplate">SQL query (can contains parameters placeholders)</param>
        /// <param name="parameters">Parameters for query</param>
        /// <param name="ignoreNulls">Null params will be ignored</param>
        Task<IEnumerable<T>> QueryManyAsync<T>(
            IMarvinReadOnlyDataContext context,
            string sqlTemplate, 
            IDictionary<string, object>? parameters = null,
            bool ignoreNulls = true);
        
        /// <summary>
        /// Executes SQl query and returns collection of value.
        /// </summary>
        /// <param name="context">Data context for query execution</param>
        /// <param name="sqlTemplate">SQL query (can contains parameters placeholders)</param>
        /// <param name="parameters">Parameters for query</param>
        Task<IEnumerable<T>> QueryManyAsync<T>(
            IMarvinReadOnlyDataContext context,
            string sqlTemplate, 
            object? parameters = null);

        /// <summary>
        /// Executes stored procedure and returns a result.
        /// </summary>
        /// <param name="context">Data context for query execution</param>
        /// <param name="procedureName">Stored procedure name</param>
        /// <param name="parameters">Parameters for stored procedure</param>
        /// <param name="timeoutSec">Timeout for stored procedure</param>
        /// <param name="ignoreNulls">Null params will be ignored</param>
        T ExecuteStoredProcedure<T>(
            IMarvinDataContext context,
            string procedureName, 
            IDictionary<string, object>? parameters = null,
            int? timeoutSec = null,
            bool ignoreNulls = true);

        /// <summary>
        /// Executes stored procedure and returns a result.
        /// </summary>
        /// <param name="context">Data context for query execution</param>
        /// <param name="procedureName">Stored procedure name</param>
        /// <param name="parameters">Parameters for stored procedure</param>
        /// <param name="timeoutSec">Timeout for stored procedure</param>
        /// <param name="ignoreNulls">Null params will be ignored</param>
        Task<T> ExecuteStoredProcedureAsync<T>(
            IMarvinDataContext context,
            string procedureName, 
            IDictionary<string, object>? parameters = null,
            int? timeoutSec = null,
            bool ignoreNulls = true);

        /// <summary>
        /// Executes stored procedure.
        /// </summary>
        /// <param name="context">Data context for query execution</param>
        /// <param name="procedureName">Stored procedure name</param>
        /// <param name="parameters">Parameters for stored procedure</param>
        /// <param name="timeoutSec">Timeout for stored procedure</param>
        /// <param name="ignoreNulls">Null params will be ignored</param>
        void ExecuteStoredProcedure(
            IMarvinDataContext context,
            string procedureName, 
            IDictionary<string, object>? parameters = null,
            int? timeoutSec = null,
            bool ignoreNulls = true);

        /// <summary>
        /// Executes stored procedure and returns a result.
        /// </summary>
        /// <param name="context">Data context for query execution</param>
        /// <param name="procedureName">Stored procedure name</param>
        /// <param name="parameters">Parameters for stored procedure</param>
        /// <param name="timeoutSec">Timeout for stored procedure</param>
        /// <param name="ignoreNulls">Null params will be ignored</param>
        Task ExecuteStoredProcedureAsync(
            IMarvinDataContext context,
            string procedureName, 
            IDictionary<string, object>? parameters = null,
            int? timeoutSec = null,
            bool ignoreNulls = true);

        /// <summary>
        /// Executes stored procedure and returns a result.
        /// </summary>
        /// <param name="context">Data context for query execution</param>
        /// <param name="procedureName">Stored procedure name</param>
        /// <param name="parameters">Parameters for stored procedure</param>
        /// <param name="timeoutSec">Timeout for stored procedure</param>
        /// <param name="ignoreNulls">Null params will be ignored</param>
        IEnumerable<T> ExecuteStoredProcedure<T>(
            IMarvinReadOnlyDataContext context,
            string procedureName, 
            IDictionary<string, object>? parameters = null,
            int? timeoutSec = null,
            bool ignoreNulls = true);

        /// <summary>
        /// Executes stored procedure and returns a result.
        /// </summary>
        /// <param name="context">Data context for query execution</param>
        /// <param name="procedureName">Stored procedure name</param>
        /// <param name="parameters">Parameters for stored procedure</param>
        /// <param name="timeoutSec">Timeout for stored procedure</param>
        /// <param name="ignoreNulls">Null params will be ignored</param>
        Task<IEnumerable<T>> ExecuteStoredProcedureAsync<T>(
            IMarvinReadOnlyDataContext context,
            string procedureName, 
            IDictionary<string, object>? parameters = null,
            int? timeoutSec = null,
            bool ignoreNulls = true);
    }
}