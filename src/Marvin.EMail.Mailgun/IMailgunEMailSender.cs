namespace Marvin.EMail.Mailgun
{
    /// <summary>
    /// Class for sending EMails via Mailgun.
    /// </summary>
    public interface IMailgunEMailSender : IEMailSender
    {
    }
}