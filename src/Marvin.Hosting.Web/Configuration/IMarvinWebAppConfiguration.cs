using Marvin.Hosting.ThreadPool;

namespace Marvin.Hosting.Web
{
    /// <summary>
    /// Basic web app configuration. 
    /// </summary>
    public interface IMarvinWebAppConfiguration : IMarvinAppConfiguration
    {
        /// <summary>
        /// Urls to listen for web app.
        /// </summary>
        public string? Urls { get; }
        
        /// <summary>
        /// Some Kestrel options.
        /// </summary>
        KestrelOptions? Kestrel { get; }
        
        /// <summary>
        /// Thread pool options.
        /// </summary>
        ThreadPoolOptions ThreadPool { get; }
    }
}