using System.Collections.Generic;
using Marvin.Configuration;
using Marvin.Hosting.ThreadPool;

namespace Marvin.Hosting.Web
{
    /// <inheritdoc cref="Marvin.Hosting.Web.IMarvinWebAppConfiguration" />
    public abstract class MarvinWebAppConfiguration : MarvinAppConfiguration, IMarvinWebAppConfiguration
    {
        /// <inheritdoc />
        public string? Urls { get; set; }

        /// <inheritdoc />
        public KestrelOptions? Kestrel { get; set; }

        public ThreadPoolOptions ThreadPool { get; set; } = new ThreadPoolOptions();

        /// <inheritdoc />
        public override IReadOnlyCollection<ConfigurationValidationError> Validate(string? prefix = null)
        {
            return ((IMarvinWebAppConfiguration) this).Validate(prefix);
        }
    }
}