using System;
using System.Collections.Generic;
using Marvin.Configuration;
using Marvin.Tools.Web;

namespace Marvin.Hosting.Web
{
    /// <summary>
    /// Validator of <see cref="IMarvinWebAppConfiguration"/> implementations.
    /// </summary>
    public static class MarvinWebAppConfigurationValidator
    {
        /// <summary>
        /// Validates implementation of <see cref="IMarvinWebAppConfiguration"/>
        /// </summary>
        public static IReadOnlyCollection<ConfigurationValidationError> Validate<T>(this T configuration, string? prefix) where T: IMarvinWebAppConfiguration
        {
            var errors = new ConfigurationValidationErrorCollection(prefix);

            if (String.IsNullOrWhiteSpace(configuration.Urls) && configuration.Kestrel == null)
            {
                errors.AddError(nameof(configuration.Urls), $"{nameof(configuration.Urls)} or {nameof(configuration.Kestrel)} must be specified");
            }
            else if (configuration.Urls != null && configuration.Kestrel != null)
            {
                errors.AddError(nameof(configuration.Urls), $"{nameof(configuration.Urls)} and {nameof(configuration.Kestrel)} can't be used at same time");
            }

            if (configuration.Kestrel != null)
            {
                errors.AddErrors(configuration.Kestrel.Validate(prefix+$":{nameof(configuration.Kestrel)}"));
            }
            
            errors.AddErrors(configuration.ThreadPool.Validate(prefix + $":{nameof(configuration.ThreadPool)}"));
            errors.AddErrors(MarvinAppConfigurationValidator.Validate(configuration, prefix));

            // ReSharper disable once SuspiciousTypeConversion.Global
            if (configuration is IWebAppConfigurationWithPublicDomain config)
            {
                errors.AddErrorIf(String.IsNullOrWhiteSpace(config.PublicDomain), nameof(config.PublicDomain), "can't be null");
            }
            
            return errors;
        }
    }
}