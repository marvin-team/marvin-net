using System;
using System.Threading;
using System.Threading.Tasks;
using Marvin.AppInitializer;
using Marvin.Tools;
using Microsoft.Extensions.Logging;

namespace Marvin.Hosting
{
    /// <summary>
    /// Initializer of <see cref="FireAndForget"/>.
    /// </summary>
    /// <remarks>
    /// Sets default logger.
    /// </remarks>
    public class FireAndForgetInitializer : IAppInitializer
    {
        private readonly ILoggerFactory _loggerFactory;

        public FireAndForgetInitializer(ILoggerFactory loggerFactory)
        {
            _loggerFactory = loggerFactory ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        /// <inheritdoc />
        public Task InitializeAsync(CancellationToken cancellationToken = default)
        {
            var logger = _loggerFactory.CreateLogger(typeof(FireAndForget));
            FireAndForget.SetLogger(logger);
            
            return Task.CompletedTask;
        }
    }
}