using Marvin.Configuration;

namespace Marvin.Hosting
{
    /// <summary>
    /// Configuration that contains EMail options for logger.
    /// </summary>
    public interface IConfigurationWithMailLogger : ILoggableOptions, IValidatableOptions
    {
        /// <summary>
        /// EMail options for logger.
        /// </summary>
        ILoggerMailOptions LoggerMail { get; }
    }
}