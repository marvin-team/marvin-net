using System.Collections.Generic;
using Marvin.Configuration;
using Marvin.Tools;

namespace Marvin.Hosting
{
    /// <inheritdoc />
    public class MarvinAppConfiguration : IMarvinAppConfiguration
    {
        /// <inheritdoc />
        public string AppName { get; set; } = null!;

        /// <inheritdoc />
        public CultureOptions Culture { get; set; } = new CultureOptions();
        
        /// <inheritdoc />
        public ILoggingConfigurationOptions Log { get; set; } = new LoggingConfigurationOptions();

        /// <inheritdoc />
        public virtual IReadOnlyCollection<ConfigurationValidationError> Validate(string? prefix = null)
        {
            return MarvinAppConfigurationValidator.Validate(this, prefix);
        }
    }
}