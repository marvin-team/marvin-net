using System.Reflection;
using EntryPoint;

namespace Marvin.Hosting
{
    /// <summary>
    /// Basic CLI arguments for any Marvin app.
    /// </summary>
    public class MarvinCLIArguments : BaseCliArguments
    {
        public MarvinCLIArguments() : this(Assembly.GetEntryAssembly()?.GetName().Name ?? "MarvinApp")
        {
        }
        
        protected MarvinCLIArguments(string utilityName) : base(utilityName)
        {
        }

        /// <summary>
        /// Absolute path to directory with app config.
        /// </summary>
        [OptionParameter(
            ShortName: 'c',
            LongName: "config")]
        [Help("Absolute path to directory with app config")]
        public string? ConfigurationDirectory { get; set; }

        /// <summary>
        /// Absolute path to config file.
        /// </summary>
        [Help("Show app version")]
        [Option(
            ShortName: 'v',
            LongName: "version")]
        public bool ShowVersion { get; set; }
    }
}