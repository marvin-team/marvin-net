namespace Marvin.Hosting
{
    public static class MarvinExitCodes
    {
        public static readonly int Success = 0;
        public static readonly int UnhandledException = 1;
        public static readonly int Cancellation = 2;
    }
}