using System;
using System.Threading;
using System.Threading.Tasks;
using Marvin.AppInitializer;
using Marvin.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;

namespace Marvin.Hosting
{
    /// <summary>
    /// Bootstrapper for simple apps that requires DI.
    /// </summary>
    public abstract class MarvinToolAppBootstrapper : MarvinToolAppBootstrapper<MarvinCLIArguments, MarvinAppConfiguration>
    {
    }
    
    /// <summary>
    /// Bootstrapper for simple apps that requires DI.
    /// </summary>
    public abstract class MarvinToolAppBootstrapper<TArgs> : MarvinToolAppBootstrapper<TArgs, MarvinAppConfiguration>
        where TArgs : MarvinCLIArguments, new()
    {
    }

    /// <summary>
    /// Bootstrapper for simple apps that requires DI.
    /// </summary>
    public abstract class MarvinToolAppBootstrapper<TArgs, TConfiguration>: MarvinAppBootstrapper<TArgs, TConfiguration> 
        where TArgs : MarvinCLIArguments, new() 
        where TConfiguration : class, IMarvinAppConfiguration, new()
    {
        private Action<IServiceCollection>? _configureServiceAction;
        private Action<IServiceCollection, TConfiguration>? _configureServiceActionWithConfig;
        
        public MarvinToolAppBootstrapper<TArgs, TConfiguration> ConfigureServices(Action<IServiceCollection> configureServiceAction)
        {
            _configureServiceAction = configureServiceAction;
            return this;
        }

        public MarvinToolAppBootstrapper<TArgs, TConfiguration> ConfigureServices(Action<IServiceCollection, TConfiguration> configureServiceAction)
        {
            _configureServiceActionWithConfig = configureServiceAction;
            return this;
        }

        /// <inheritdoc />
        protected override async Task<int> RunInternalAsync(
            string[] rawArguments,
            TArgs arguments,
            TConfiguration configuration,
            IConfigurationProvider<TConfiguration> configurationProvider,
            string customContentRootDirectory,
            CancellationToken cancellationToken = default)
        {
            // configure service
            var services = new ServiceCollection();
            services.AddSingleton<ILogger>(c =>
            {
                var loggerFactory = c.GetRequiredService<ILoggerFactory>();
                return loggerFactory.CreateLogger("main");
            });
            services.AddLocalization(opt =>
            {
                opt.ResourcesPath = "Resources";
            });
            _configureServiceAction?.Invoke(services);
            _configureServiceActionWithConfig?.Invoke(services, configuration);
            services.AddAppInitialization();
                
            services.TryAddSingleton(configuration);
            services.TryAddSingleton(arguments);
            services.TryAddSingleton(configurationProvider);

            services.AddAppInitializer<FireAndForgetInitializer>();
            services.AddLogging(opt =>
            {
                opt.ClearProviders();
                opt.SetMinimumLevel(LogLevel.Trace);
                opt.AddNLog();
            });

            var sp = services.BuildServiceProvider();
            using (var scope = sp.CreateScope())
            {
                await scope.InitAsync(cancellationToken);

                return await ExecuteAsync(sp, rawArguments, arguments, configuration, cancellationToken);
            }
        }

        /// <summary>
        /// Executes specified tool's actions.
        /// </summary>
        protected abstract Task<int> ExecuteAsync(
            IServiceProvider serviceProvider, 
            string[] rawArguments,
            TArgs arguments,
            TConfiguration configuration,
            CancellationToken cancellationToken = default);
    }
}