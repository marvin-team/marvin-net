using Marvin.Configuration;

namespace Marvin.Tools.Web
{
    public interface IWebAppConfigurationWithPublicDomain : ILoggableOptions, IValidatableOptions
    {
        /// <summary>
        /// Public address to access web app.
        /// </summary>
        public string PublicDomain { get; }
    }
}