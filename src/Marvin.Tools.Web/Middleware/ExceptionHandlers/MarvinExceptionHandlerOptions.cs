using Microsoft.AspNetCore.Http;

namespace Marvin.Tools.Web.Middleware
{
    /// <summary>
    /// Options for <see cref="MarvinExceptionHandleMiddleware"/>
    /// </summary>
    public class MarvinExceptionHandlerOptions
    {
        /// <summary>
        /// Path with action that will handle exception
        /// </summary>
        public PathString ExceptionHandlingPath { get; set; }
    }
}