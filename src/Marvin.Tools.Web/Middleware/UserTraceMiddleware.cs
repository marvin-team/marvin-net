using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Marvin.Tools.Web.Middleware
{
    /// <summary>
    /// Middleware for creating unique id for each user without authenticating.
    /// </summary>
    //todo #5
    public class UserTraceMiddleware
    {
        private const string UserTraceCookieName = "marvin-user-trace-id";
        
        private readonly RequestDelegate _next;

        public UserTraceMiddleware(RequestDelegate next)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
        }
        
        public async Task InvokeAsync(HttpContext context)
        {
            long? userTraceId = null;
            // try to fetch user trace id from cookies
            // if no such cookie, create a new one
            if (!context.Request.Cookies.TryGetValue(UserTraceCookieName, out var userTraceIdCookieValue) || !PublicId.TryParse(userTraceIdCookieValue, out var temp))
            {
                userTraceId = UniqueIdGenerator.Generate();

                context.Response.Cookies.Append(
                    UserTraceCookieName,
                    userTraceId.Value.ToPublicId(),
                    new CookieOptions
                    {
                        SameSite = SameSiteMode.None,
                        IsEssential = true,
                        MaxAge = TimeSpan.FromDays(365)
                    });
            }

            await _next(context);
        }
    }
}