namespace Marvin.Tools.Web.Pagination
{
    public delegate string PaginationGetUrlForPageDelegate(int page);
}