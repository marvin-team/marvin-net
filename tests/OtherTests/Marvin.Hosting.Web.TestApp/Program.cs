using System.Threading.Tasks;

namespace Marvin.Hosting.Web.TestApp
{
    public class Configuration : MarvinWebAppConfiguration
    {
        public Configuration()
        {
            AppName = "Test App";
        }
    }

    public class Program
    {
        public class CliArgs : MarvinCLIArguments
        {
            public CliArgs() : base("test")
            {
            }
        }

        public static async Task Main(string[] args)
        {
            var bootstrapper = new MarvinWebAppBootstrapper<CliArgs, Configuration, Startup>();

            await bootstrapper.RunAsync(args);
        }
    }
}